package com.geopagos.prueba.figuras.services;

import com.geopagos.prueba.figuras.data.entities.Geometrico;
import com.geopagos.prueba.figuras.exception.FiguraNotFoundException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Leonardo
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FiguraServiceTest {

    @Autowired
    private FiguraService service;

    @Test
    public void GivenFiguraService_WhenAddTrianguloValidateSuccess_ThenOk()
            throws FiguraNotFoundException {
        Geometrico triangulo = service.add("triangulo", 10D, 10D, null);
        Assert.assertTrue(triangulo.getId() > 0);
        Assert.assertNotNull(triangulo);
        Assert.assertEquals(triangulo.getAltura(), new Double(10));
        Assert.assertEquals(triangulo.getBase(), new Double(10));
        Assert.assertNull(triangulo.getDiametro());
        Assert.assertEquals(triangulo.calcularSuperficie(), new Double(10 * (10 / 2)));
    }

    @Test
    public void GivenFiguraService_WhenAddCuadradoValidateSuccess_ThenOk()
            throws FiguraNotFoundException {
        Geometrico cuadrado = service.add("cuadrado", 10D, null, null);
        Assert.assertTrue(cuadrado.getId() > 0);
        Assert.assertNotNull(cuadrado);
        Assert.assertNull(cuadrado.getAltura());
        Assert.assertEquals(cuadrado.getBase(), new Double(10));
        Assert.assertNull(cuadrado.getDiametro());
        Assert.assertEquals(cuadrado.calcularSuperficie(), new Double(10 * 10));
    }

    @Test
    public void GivenFiguraService_WhenAddCirculoValidateSuccess_ThenOk()
            throws FiguraNotFoundException {
        Geometrico circulo = service.add("circulo", null, null, 20D);
        Assert.assertNotNull(circulo);
        Assert.assertTrue(circulo.getId() > 0);
        Assert.assertNull(circulo.getAltura());
        Assert.assertNull(circulo.getBase());
        Assert.assertEquals(circulo.getDiametro(), new Double(20));
        Assert.assertEquals(
                circulo.calcularSuperficie(),
                new Double(Math.PI * Math.pow((double) circulo.getDiametro() / 2, 2)));
    }

    @Test(expected = FiguraNotFoundException.class)
    public void GivenFiguraService_WhenAddPentagonoValidateFiguraNotFound_ThenThrowException()
            throws FiguraNotFoundException {
        Geometrico pentagono = service.add("pentagono", 20D, 20D, 20D);
    }

    @Test
    public void GivenFiguraService_WhenFindCirculoValidateSuccess_ThenOk()
            throws FiguraNotFoundException {
        Geometrico circulo = service.add("circulo", null, null, 20D);
        Assert.assertNotNull(circulo);
        Assert.assertTrue(circulo.getId() > 0);
        Assert.assertNull(circulo.getAltura());
        Assert.assertNull(circulo.getBase());
        Assert.assertEquals(circulo.getDiametro(), new Double(20));
        Assert.assertEquals(
                circulo.calcularSuperficie(),
                new Double(Math.PI * Math.pow((double) circulo.getDiametro() / 2, 2)));
        Geometrico circuloExistente = service.find(circulo.getId(), "circulo");
        Assert.assertNotNull(circuloExistente);
        Assert.assertEquals(circulo.getId(), circuloExistente.getId());
        Assert.assertNull(circuloExistente.getAltura());
        Assert.assertNull(circuloExistente.getBase());
        Assert.assertEquals(circulo.getDiametro(), circuloExistente.getDiametro());
    }

    @Test(expected = FiguraNotFoundException.class)
    public void GivenFiguraService_WhenFindCirculoValidateFiguraNotFound_ThenThrowException()
            throws FiguraNotFoundException {
        Geometrico circulo = service.find(99999, "circulo");
    }

    @Test
    public void GivenFiguraService_WhenUpdateValidateSuccess_ThenOk()
            throws FiguraNotFoundException {
        Geometrico cuadrado = service.add("cuadrado", 15D, null, null);
        Assert.assertNotNull(cuadrado);
        Assert.assertTrue(cuadrado.getId() > 0);
        Assert.assertNotNull(cuadrado.getBase());
        Assert.assertNull(cuadrado.getAltura());
        Assert.assertNull(cuadrado.getDiametro());

        Long idActual = cuadrado.getId();
        Double baseAnterior = cuadrado.getBase();
        Double baseNueva = 25D;
        Double superficieAnterior = cuadrado.calcularSuperficie();

        cuadrado.setBase(baseNueva);

        cuadrado = service.update(cuadrado);
        Assert.assertNotNull(cuadrado);
        Assert.assertEquals(cuadrado.getId(), idActual);
        Assert.assertEquals(baseNueva, cuadrado.getBase());
        Assert.assertNotEquals(superficieAnterior, cuadrado.calcularSuperficie());
        Assert.assertNotEquals(cuadrado.getBase(), baseAnterior);
    }

}
