package com.geopagos.prueba.figuras.services;

import com.geopagos.prueba.figuras.data.entities.Geometrico;
import com.geopagos.prueba.figuras.exception.FiguraNotFoundException;

/**
 *
 * @author Leonardo
 */
public interface FiguraService {

    /**
     * Obtiene la informacion de una figura geometrica dados los parametros de
     * entrada
     *
     * @param id el ID de la figura a consultar
     * @param tipo el tipo de figura
     * @return un Geometrico con la informacion solicitada.
     * @throws FiguraNotFoundException si no se halla ningun registro.
     */
    Geometrico find(long id, String tipo) throws FiguraNotFoundException;

    /**
     * Agrega una figura
     *
     * @param tipoFigura el tipo de figura a agregar
     * @param base la base (si aplica)
     * @param altura la altura (si aplica)
     * @param diametro el diametro (si aplica)
     * @return la informacion de la figura solicitada
     * @throws FiguraNotFoundException si Factory no encuentra una clase acorde
     * a la solicitada.
     */
    Geometrico add(String tipoFigura, Double base, Double altura,
            Double diametro) throws FiguraNotFoundException;

    /**
     * Actualiza la informacion de una figura existente
     *
     * @param figura la figura a modificar
     * @return la informacion de la figura satisfactoriamente actualizada
     * @throws FiguraNotFoundException si la figura no fue hallada.
     */
    Geometrico update(Geometrico figura) throws FiguraNotFoundException;

}
