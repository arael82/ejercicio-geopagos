package com.geopagos.prueba.figuras.services.impl;

import com.geopagos.prueba.figuras.data.entities.Circulo;
import com.geopagos.prueba.figuras.data.entities.Cuadrado;
import com.geopagos.prueba.figuras.data.entities.Geometrico;
import com.geopagos.prueba.figuras.data.entities.Triangulo;
import com.geopagos.prueba.figuras.data.repository.CirculoRepository;
import com.geopagos.prueba.figuras.data.repository.CuadradoRepository;
import com.geopagos.prueba.figuras.data.repository.TrianguloRepository;
import com.geopagos.prueba.figuras.exception.FiguraNotFoundException;
import com.geopagos.prueba.figuras.factory.FiguraFactory;
import com.geopagos.prueba.figuras.services.FiguraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Leonardo
 */
@Service
public class FiguraServiceImpl implements FiguraService {

    @Autowired
    private FiguraFactory factory;

    @Autowired
    private TrianguloRepository tRepository;
    @Autowired
    private CuadradoRepository cRepository;
    @Autowired
    private CirculoRepository ciRepository;

    /**
     * Obtiene la informacion de una figura geometrica dados los parametros de
     * entrada
     *
     * @param id el ID de la figura a consultar
     * @param tipo el tipo de figura
     * @return un Geometrico con la informacion solicitada.
     * @throws FiguraNotFoundException si no se halla ningun registro.
     */
    @Override
    public Geometrico find(long id, String tipo) throws FiguraNotFoundException {
        /**
         * Se intentara obtener la figura de la tabla correspondiente, de
         * acuerdo al tipo;
         *
         */
        Geometrico figura = null;

        switch (tipo.toLowerCase()) {
            case "circulo":
                figura = (Geometrico) ciRepository.findOneById(id);
                break;
            case "cuadrado":
                figura = (Geometrico) cRepository.findOneById(id);
                break;
            case "triangulo":
            default:
                figura = (Geometrico) tRepository.findOneById(id);
        }

        if (figura != null) {
            return figura;
        } else {
            throw new FiguraNotFoundException("Figura no encontrada");
        }
    }

    /**
     * Agrega una figura
     *
     * @param tipoFigura el tipo de figura a agregar
     * @param base la base (si aplica)
     * @param altura la altura (si aplica)
     * @param diametro el diametro (si aplica)
     * @return la informacion de la figura solicitada
     * @throws FiguraNotFoundException si Factory no encuentra una clase acorde
     * a la solicitada.
     */
    @Override
    public Geometrico add(String tipoFigura, Double base, Double altura, Double diametro) throws FiguraNotFoundException {

        Geometrico figura = this.factory.getFigura(tipoFigura);

        try {
            if (base != null) {
                figura.setBase(base);
            }
        } catch (UnsupportedOperationException uoEx) {
        }

        try {
            if (altura != null) {
                figura.setAltura(altura);
            }
        } catch (UnsupportedOperationException uoEx) {
        }

        try {
            if (diametro != null) {
                figura.setDiametro(diametro);
            }
        } catch (UnsupportedOperationException uoEx) {
        }

        figura.dibujar();

        switch (tipoFigura.toLowerCase()) {
            case "circulo":
                figura = ciRepository.saveAndFlush((Circulo) figura);
                break;
            case "cuadrado":
                figura = cRepository.saveAndFlush((Cuadrado) figura);
                break;
            case "triangulo":
                figura = tRepository.saveAndFlush((Triangulo) figura);
            default:

        }

        return figura;

    }

    /**
     * Actualiza la informacion de una figura existente
     *
     * @param figura la figura a modificar
     * @return la informacion de la figura satisfactoriamente actualizada
     * @throws FiguraNotFoundException si la figura no fue hallada.
     */
    @Override
    public Geometrico update(Geometrico figura) throws FiguraNotFoundException {

        if (figura.getId() < 1) {
            throw new FiguraNotFoundException("El ID de la figura no es valido.");
        }

        Geometrico figuraFound = this.find(figura.getId(),
                figura.getClass().getSimpleName().toLowerCase());

        if (figuraFound != null) {
            switch (figura.getClass().getSimpleName().toLowerCase()) {
                case "circulo":
                    figura = ciRepository.saveAndFlush((Circulo) figura);
                    break;
                case "cuadrado":
                    figura = cRepository.saveAndFlush((Cuadrado) figura);
                    break;
                case "triangulo":
                    figura = tRepository.saveAndFlush((Triangulo) figura);
                default:

            }
        }

        return figura;

    }

}
