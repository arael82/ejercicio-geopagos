package com.geopagos.prueba.figuras.controller;

import com.geopagos.prueba.figuras.data.entities.Geometrico;
import com.geopagos.prueba.figuras.exception.FiguraNotFoundException;
import com.geopagos.prueba.figuras.services.FiguraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Leonardo
 */
@RestController
@RequestMapping(value = "/figura")
public class FiguraController {

    @Autowired
    private FiguraService service;

    /**
     * Permite crear una nueva figura
     *
     * @param tipoFigura el tipo de figura que se desea crear.
     * @param base la base (si aplica)
     * @param altura la altura (si aplica)
     * @param diametro el diámetro (si aplica)
     * @return la información de la figura modificada satisfactoriamente
     * @throws FiguraNotFoundException si la figura no pudo ser validada
     * correctamente en su creación
     */
    @RequestMapping(value = "/", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Geometrico> crear(
            @RequestParam(required = true, name = "tipoFigura") String tipoFigura,
            @RequestParam(required = false, name = "b") Double base,
            @RequestParam(required = false, name = "h") Double altura,
            @RequestParam(required = false, name = "d") Double diametro
    ) throws FiguraNotFoundException {
        Geometrico result = this.service.add(tipoFigura, base, altura, diametro);

        return new ResponseEntity(result, HttpStatus.OK);
    }

    /**
     * Permite visualizar la información de una figura geométrica
     *
     * @param id el ID del registro
     * @param tipoFigura el tipo de figura esperado
     * @return la información de la figura solicitada
     * @throws FiguraNotFoundException si no se encontró el registro
     */
    @RequestMapping(value = "/{tipo}/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Geometrico> ver(
            @PathVariable(name = "id") long id,
            @PathVariable(name = "tipo") String tipoFigura
    ) throws FiguraNotFoundException {
        Geometrico result = this.service.find(id, tipoFigura);
        return new ResponseEntity(result, HttpStatus.OK);
    }

    /**
     * Permite actualizar la información de una figura existente.
     *
     * @param figura la figura a modificar, con sus datos actualizados.
     * @return un objeto representando la figura actualizada
     * @throws FiguraNotFoundException si no se encontró la figura solicitada.
     */
    @RequestMapping(value = "/", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Geometrico> editar(
            @RequestBody(required = true) Geometrico figura
    ) throws FiguraNotFoundException {
        Geometrico result = this.service.update(figura);

        return new ResponseEntity(result, HttpStatus.OK);
    }

}
