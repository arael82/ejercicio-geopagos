package com.geopagos.prueba.figuras.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Leonardo
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class FiguraNotFoundException extends Exception {

    public FiguraNotFoundException(String message) {
        super(message);
    }

}
