package com.geopagos.prueba.figuras.data.repository;

import com.geopagos.prueba.figuras.data.entities.Triangulo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Leonardo
 */
public interface TrianguloRepository extends JpaRepository<Triangulo, Long> {

    Triangulo findOneById(long id);
}
