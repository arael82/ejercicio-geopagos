package com.geopagos.prueba.figuras.data.repository;

import com.geopagos.prueba.figuras.data.entities.Circulo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Leonardo
 */
public interface CirculoRepository extends JpaRepository<Circulo, Long> {

    Circulo findOneById(long id);
}
