package com.geopagos.prueba.figuras.data.entities;

import java.io.Serializable;
import javax.persistence.Entity;

/**
 *
 * @author Leonardo
 */
@Entity(name="cuadrado")
@SuppressWarnings("PersistenceUnitPresent")
public class Cuadrado extends AbstractFigura implements Geometrico, Serializable {

    private Double base;

    @Override
    public Double calcularSuperficie() {
        return Math.pow(this.base,2);
    }

    @Override
    public Double getBase() {
        return this.base;
    }

    @Override
    public void setBase(Double base) {
        this.base = base;
    }

    @Override
    public Double getAltura() {
        return null;
    }

    @Override
    public Double getDiametro() {
        return null;
    }
    
}
